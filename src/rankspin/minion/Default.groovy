package rankspin.minion

import rankspin.Dispatch
import rankspin.util.MyServletRequest
import javax.servlet.http.HttpServletResponse


// Called when the requested Minion does not exist
class Default extends Minion {
  @Override
  void go(MyServletRequest req, HttpServletResponse res) {
    res.contentType = "text/html"
    res.writer.println("Minion of name '${req[Dispatch.REQ_MINION_NAME]}' not found")
  }
}
