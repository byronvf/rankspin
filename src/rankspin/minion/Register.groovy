package rankspin.minion

import rankspin.entity.Player
import groovy.transform.CompileStatic
import rankspin.util.EmHelper
import rankspin.util.MyServletRequest

import javax.servlet.http.HttpServletResponse

@CompileStatic
class Register extends Minion {
  @Override
  def void go(MyServletRequest req, HttpServletResponse res) {
    def player = new Player(email:req.get("email"),
        firstname:req.get("firstname"),
        lastname:req.get("lastname"),
        nickname:req.get("nickname"))
    def em = EmHelper.entityManager

    EmHelper.db {
      em.persist(player)
    }
    // def rankspin.entity.Player.Gender = req["gender"]
  }
}
