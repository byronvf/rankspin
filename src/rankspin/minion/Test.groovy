package rankspin.minion

import rankspin.entity.Event
import rankspin.entity.Game
import rankspin.entity.Player
import rankspin.entity.PlayerEvent
import groovy.transform.CompileStatic
import rankspin.util.EmHelper
import rankspin.util.MyServletRequest

import javax.servlet.http.HttpServletResponse

/**
 * Created by byron on 2014-11-19.
 */
@CompileStatic
class Test extends Minion {
  @Override
  def void go(MyServletRequest req, HttpServletResponse res) {
    res.contentType = "text/html"
    res.writer.println("I am here")
    def em = EmHelper.entityManager

    def game = new Game(name:"vball")
    def player = new Player(email:"my@me.com")
    def event = new Event(game:game)
    def playerEvent = new PlayerEvent(player:player, event:event, result:PlayerEvent.Result.WIN)

    EmHelper.db {
      em.persist(player)
      em.persist(event)
      em.persist(playerEvent)
      em.persist(game)

      def ev = EmHelper.get(Event, "2015-06-14 00:45:44.305011")
    }
  }
}
