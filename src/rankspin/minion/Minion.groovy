package rankspin.minion

import groovy.transform.CompileStatic
import rankspin.util.MyServletRequest

import javax.servlet.http.*

@CompileStatic
class Minion {

  def String name

  def void go (MyServletRequest req, HttpServletResponse res) {
    res.contentType = "text/html"
    res.writer.println("I have nothing to do!")
  }
 }