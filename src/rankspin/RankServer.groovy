package rankspin

import groovy.util.logging.Log
import org.eclipse.jetty.annotations.AnnotationConfiguration
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.handler.ContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.util.log.JavaUtilLog
import org.eclipse.jetty.webapp.WebAppContext
import org.eclipse.persistence.config.PersistenceUnitProperties as PUP
import rankspin.entity.TimestampSequence
import rankspin.util.EmHelper
import rankspin.util.MyCustomizer
import rankspin.util.MyLogFormatter
import rankspin.util.ThrowablePrinter

import javax.persistence.Persistence
import javax.security.auth.login.Configuration
import java.util.logging.ConsoleHandler
import java.util.logging.Level
import java.util.logging.LogManager
import java.util.logging.Logger

@Log
class RankServer {

  def initLogging() {
    def logMgr = LogManager.logManager
    def rootLog = logMgr.getLogger("")
    def handler = new ConsoleHandler()
    handler.formatter = new MyLogFormatter()
    handler.level = Level.ALL
    rootLog.level = Level.INFO
    for (h in rootLog.handlers) {rootLog.removeHandler(h)} // Remove any default handlers
    rootLog.addHandler(handler)
    log.info("Logging init done")
  }

  def initJetty() {
    org.eclipse.jetty.util.log.Log.setLog(new JavaUtilLog())
    Logger.getLogger('org.eclipse.jetty').level = Level.INFO
    def server = new Server(8000)

    WebAppContext webapp = new WebAppContext()
    webapp.setContextPath("/")
    webapp.setWar("war")
    webapp.addServlet(new ServletHolder(new Dispatch()), "/do/*");
    server.setHandler(webapp)

//    def chandler = new ContextHandler("/")
//    chandler.handler = new Dispatch()
//    server.handler = chandler

    server.start()

    log.info("Jetty init done")
  }

  def initEclipseLink() {
    def props = new HashMap();
    props.put(PUP.TARGET_DATABASE, "PostgreSQL")
    props.put(PUP.JDBC_DRIVER, "org.postgresql.Driver")
//    For JDBC driver looging
//    DriverManager.logStream = System.out
//    props.put(PUP.JDBC_URL, "jdbc:postgresql://localhost:5432/rankspin?stringtype=unspecified&loglevel=2")
    props.put(PUP.JDBC_URL, "jdbc:postgresql://localhost:5432/rankspin")
    props.put(PUP.JDBC_USER, "byron")
    props.put(PUP.JDBC_PASSWORD, "")
    props.put(PUP.PERSISTENCE_CONTEXT_FLUSH_MODE, "COMMIT")
    props.put(PUP.SESSION_CUSTOMIZER, MyCustomizer.class.name)
    props.put(PUP.LOGGING_LOGGER, "JavaLogger")  // Use java.rankspin.util.logging
    props.put(PUP.LOGGING_LEVEL, "CONFIG")
    props.put(PUP.LOGGING_PARAMETERS, "false")
    props.put('eclipselink.logging.level.sql', 'INFO') // SQL logging is independent

    def emf = Persistence.createEntityManagerFactory("blowme", props)
    EmHelper.init(emf) // Init the helper class for easy access to EMs
    log.info("Eclipse init done")
  }

  def init() {
    try {
      initLogging()
      initJetty()
      initEclipseLink()
    } catch (Throwable e) {
      log.severe((new ThrowablePrinter(e).toString()))
      log.severe("Server initialization failed, so good bye!")
      System.exit(1)
    }

//      LogManager.getLogManager().loggerNames.each {
//      def logger = LogManager.getLogManager().getLogger(it)
//      println "logger '$logger.name' level: '$logger.level'"
//    }
  }

  public static void main(String[] args) {
    def server = new RankServer()
    server.init()
  }
}

