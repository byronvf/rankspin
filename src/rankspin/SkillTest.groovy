package rankspin

import jskills.GameInfo
import jskills.Rating
import jskills.Team
import jskills.TrueSkillCalculator
import rankspin.entity.Player


def player1 = new Player(id:1)
def player2 = new Player(id:2)
def player3 = new Player(id:3)
def player4 = new Player(id:4)

def team1 = new Team()
def team2 = new Team()

team1.addPlayer(player1, new Rating(500, 30))
team1.addPlayer(player2, new Rating(500, 30))

team2.addPlayer(player3, new Rating(500, 30))
team2.addPlayer(player4, new Rating(500, 30))

def results = TrueSkillCalculator.calculateNewRatings(GameInfo.defaultGameInfo, [team1, team2], 1, 2)

println "player1: " + results[player1]
println "player2: " + results[player2]
println "player3: " + results[player3]
println "player4: " + results[player4]

team1 = new Team()
team2 = new Team()

team1.addPlayer(player1, results[player1])
team1.addPlayer(player3, results[player3])

team2.addPlayer(player2, results[player2])
team2.addPlayer(player4, results[player4])

results = TrueSkillCalculator.calculateNewRatings(GameInfo.defaultGameInfo, [team1, team2], 1, 2)

println "player1: " + results[player1]
println "player2: " + results[player2]
println "player3: " + results[player3]
println "player4: " + results[player4]
