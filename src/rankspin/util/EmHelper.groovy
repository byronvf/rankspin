package rankspin.util

import groovy.transform.CompileStatic
import groovy.util.logging.Log

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory
import javax.persistence.Query
import java.sql.Timestamp;
/**
 * @author MyEclipse Persistence Tools
 */


@Log
@CompileStatic
class EmHelper {

  private static EntityManagerFactory emf
  private static ThreadLocal<EntityManager> emThreadLocal = new ThreadLocal<>()
  private static ThreadLocal<Integer> commitLevelThreadLocal = new ThreadLocal<>()

  static void init(EntityManagerFactory emff) {
    emf = emff
  }

  static EntityManager getEntityManager() {
    EntityManager manager = emThreadLocal.get()
    if (manager == null || !manager.isOpen()) {
      manager = emf.createEntityManager();
      emThreadLocal.set(manager);
      commitLevelThreadLocal.set(0)
    }
    return manager;
  }

  int getCommitLevel() {
    return commitLevelThreadLocal.get()
  }

  static void closeEntityManager() {
    EntityManager em = emThreadLocal.get();
    emThreadLocal.set(null);
    commitLevelThreadLocal.set(0)
    em?.close();
  }

  static void beginTransaction() {
    getEntityManager().getTransaction().begin();
    def level = commitLevelThreadLocal.get()+1
    commitLevelThreadLocal.set(level)
    log.fine("transaction begin $level")
  }

  static void commit() {
    def level = commitLevelThreadLocal.get()
    log.fine("transaction commit $level")
    level--
    commitLevelThreadLocal.set(level)
    assert level >= 0
    if (level == 0) {
      getEntityManager().getTransaction().commit();
    }
  }

  public static void rollback() {
    log.fine("transaction rollback")
    def level = commitLevelThreadLocal.get() - 1
    commitLevelThreadLocal.set(level)
    assert level >= 0
    if (level == 0) {
      getEntityManager().getTransaction().rollback();
    }
  }

  static Query createQuery(String query) {
    return getEntityManager().createQuery(query);
  }

  static <T> T get(Class<T> clazz, Timestamp id) {
    return getEntityManager().find(clazz, id);
  }

  static <T> T get(Class<T> clazz, String timestr) {
    def id = Timestamp.valueOf(timestr)
    return getEntityManager().find(clazz, id);
  }

  static void db(Closure transact) {
    beginTransaction()
    try {
      transact()
    } catch (Exception e) {
      rollback()
      throw e;
    }
    commit()
  }
}

