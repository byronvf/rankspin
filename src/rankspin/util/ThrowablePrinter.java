package rankspin.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class ThrowablePrinter
{
  Logger log = Logger.getLogger(this.getClass().getName());
  public static final String BASE_PACKAGE_NAME = "cc.base2.socrates";
  public boolean error = false;  // Set to true if the exception chain can't be processed.
  public boolean stackerror = false;
  public String errmsg = null;
  boolean removeWebServerTrace = false;
  Throwable root = null;
  ArrayList<ExInf> exlist = new ArrayList<ExInf>();
  
  /**
   * Object that contains each exception in the exception chain.
   */
  public static class ExInf
  {
    StackTraceElement stack[] = null;
    Throwable ex = null;
    String msg = null;
    StackTraceElement thrownAt = null;  // The location where the exception was thrown
    int appidx = -1;  // Index of stack which contains the first 
                      // stack element of this application
    
    public String getMsg()
    {
      return msg;
    }
    
    public StackTraceElement[] getStack()
    {
      return stack;
    }
    
    public Throwable getEx()
    {
      return ex;
    }
    
    public int getAppIdx()
    {
      return appidx;
    }
    
    public StackTraceElement getThrownAt()
    {
      return thrownAt;
    }
  }
  
  public ThrowablePrinter(Throwable t, boolean removeWebServerTrace)
  {
    this.removeWebServerTrace = removeWebServerTrace;
    this.root = t;
    try
    {
      init();
    }
    catch(Throwable e)
    {
      // If an exception occurs while trying to unwind this exception, then we 
      // give up on it, and we will end up reporting the exception in the
      // standard way... printStackTrace().
      error = true;
      errmsg = e.getMessage();
    }
  }
    
  public ThrowablePrinter(Throwable t)
  {
    this(t, true);
  }
  
  public void init()
  {
    // Create a list of all the exceptions
    Throwable currex = root;
    int prevsize = 0;
    while (currex != null)
    {
      ExInf exinf = new ExInf();
      exinf.ex = currex;
      exlist.add(exinf);

      StackTraceElement currstack[] = currex.getStackTrace();
      int size = currstack.length - prevsize - 1;
      

      if (size >= 0)
        exinf.stack = copyStack(currstack, 1, size);
      else
      {
        // If size is less than one, then the exception was wrapped and
        // moved to a different stack level then where it was caught.
        // Its difficult to determine how deep and remove irrelevant stack
        // levels without allot of analysis, we set the error flag to true
        // and do our best to show some lines of stack.
        
        // Note that we originally just exceptioned out, and printed the stack
        // in the regular way, however.. We found that certain PSQLExceptions
        // infact cause this error (Shame on Postgres) and that the regular 
        // ex.printStackTrace does not show the complete chain, so this 
        // at least will show more info.
        stackerror = true;
        exinf.stack = copyStack(currstack, 0, currstack.length < 10 ? currstack.length : 10);
      }
      
      exinf.msg = currex.getMessage();
      // The first element is always where the throw clause was located
      exinf.thrownAt = currstack[0];
      

      // If we can use getCause, then we try other methods. Unfortunetly, Some exception
      // types have specific ways of getting the nested exception.
      if (currex.getCause() != null)
      {
        currex = currex.getCause();  
      }
      else if (currex instanceof InvocationTargetException &&
          ((InvocationTargetException)currex).getTargetException() != null)
      {
        currex = ((InvocationTargetException)currex).getTargetException();
      }
      else if (currex instanceof java.sql.BatchUpdateException)
      {
        currex = ((java.sql.BatchUpdateException)currex).getNextException();
      }
      else
      {
        currex = null;
      }
      
      prevsize = currstack.length - 1;
    }

    if (removeWebServerTrace)
    {
      // Remove all the stack trace crud from the servlet container, this
      // will always, of corse, be in the first exception of the chain.
      ExInf exinf = exlist.get(0);
      int i = 0;
      while (i < exinf.stack.length)
      {
        if (exinf.stack[i++].getClassName().contains(cutOffClassName))
          break;
      }
      exinf.stack = copyStack(exinf.stack, 0, i);
    }

    Collections.reverse(exlist);

    found:
    for (ExInf exinf : exlist)
    {
      for (int i = 0; i < exinf.stack.length; i++)
      {
        if (exinf.stack[i].toString().contains(BASE_PACKAGE_NAME))
        {
          exinf.appidx = i;
          break found;
        }
      }
    }    
  }

  // Do not display any stack lines from this class name down.
  // We use this for brevity in the stack display since we don't care about
  // all the servlet engine call levels.
  public static final String cutOffClassName = "javax.servlet.http.HttpServlet";

  public StackTraceElement[] copyStack(StackTraceElement[] stack, int begin, int size)
  {
    StackTraceElement selms[] = new StackTraceElement[size];
    for (int i=0; i < size; i++)
    {
      selms[i] = stack[i+begin];
    }        
    return selms;
  }
  
  public List<ExInf> getExInfs()
  {
    return exlist;
  }

  public void out(Writer w) throws IOException
  {
    if (error)
    {
      w.write("ERROR, The exception could not be unwound, defaulting to standard exception reporting");
      w.write("Cause of ERROR: " + errmsg);
      root.printStackTrace(new PrintWriter(w));
      return;
    }
    
    if (stackerror)
    {
      w.write("Warning:  The stack chain was not aligned!!!\n");
    }
    
    for (ExInf exinf : exlist)
    {
      w.write("Threw: ");
      w.write(exinf.ex.getClass().getName());
      w.write("\n");
      w.write("msg: ");
      if (exinf.getMsg() != null)
        w.write(exinf.getMsg());
      else
        w.write("<no message>");
      w.write("\n");
      w.write("thrown at:  \n    " + exinf.thrownAt.toString());
      w.write("\n");
      for (int j = 0; j < exinf.stack.length; j++)
      {
        if (exinf.appidx == j)
          w.write(" -> ");
        else
          w.write("    ");
        
        w.write(exinf.stack[j].toString());
        w.write("\n");
      }
    }
  }
  
  /**
   * Number of exceptions in the chain.
   */
  public int size()
  {
    return exlist.size();
  }
  
  public String toString()
  {
    StringWriter sw = new StringWriter();
    try
    {
      out(sw);
    } 
    catch (IOException e)
    {
      throw new RuntimeException(e);
    }
    return sw.toString();
  }
  
  public void logToError(Logger log, String msg)  
  {
    log.severe(msg + "\n\n" + toString());
  }
  
  public boolean getError()
  {
    return error;
  }

//  public String getFilePath(StackTraceElement elm)
//  {
//    if (elm.getClassName().startsWith(BASE_PACKAGE_NAME))
//    {
//      StringBuffer buf = new StringBuffer();
//      buf.append(Global.docRootDir.getPath());
//      buf.append(File.separatorChar).append("..").append(File.separatorChar);
//      buf.append(elm.getClassName().replace('.', File.separatorChar));
//      buf.append(".java");
//      String str = buf.toString();
//      return str;
//    }
//    return null;
//  }
}
