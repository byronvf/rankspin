package rankspin.util

import org.eclipse.persistence.mappings.DirectToFieldMapping
import org.eclipse.persistence.sessions.Session
import org.eclipse.persistence.sessions.factories.SessionCustomizer
import rankspin.entity.TimestampSequence

/*
  This mess is to convert camel case class and field names to lowercase with under
  underscore names for tables and columns names respectively in the database.  Lets
  hope it doesn't fuck anything up!

  Note - Verified that this is only run one time to make the mapping.
 */
public class MyCustomizer implements SessionCustomizer {

  @Override
  public void customize( Session session ) throws Exception {

    session.getLogin().addSequence(new TimestampSequence("id_gen"))

    session.descriptors.values().each { desc ->
      // Update table names
      desc.tableName = camelCaseToUnderscore(desc.javaClass.simpleName)

      // build name maps
      def nameMap = new HashMap<String, String>();
      String prefix = "${desc.tableName}.";
      desc.javaClass.declaredFields.each { field ->
        String key = prefix + field.name.toUpperCase();
        String value = prefix + camelCaseToUnderscore(field.name);
        nameMap.put(key, value);
      }

      desc.mappings.each { mapping ->
        if (mapping.isDirectToFieldMapping()) {
          DirectToFieldMapping directMapping = (DirectToFieldMapping)mapping;
          String oldFieldName = directMapping.getFieldName(); // format: table_name.FIELD
          directMapping.getField().resetQualifiedName(nameMap.get(oldFieldName));
        }
      }
    }
  }


  private static String camelCaseToUnderscore( String camelCase ) {
    return camelCase.trim().replaceAll(/(?<!^)[A-Z](?!$)/, {"_$it"}).toLowerCase();
  }
}