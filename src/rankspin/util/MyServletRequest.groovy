package rankspin.util

import groovy.transform.InheritConstructors

import javax.servlet.ServletRequestWrapper

/**
 * Created by byron on 2014-12-02.
 */
@InheritConstructors
class MyServletRequest extends ServletRequestWrapper {
  String get(String name) {
    String val = getRequest().getParameter(name)
    if (val == null)
      val = getAttribute(name)
    return val
  }

  void set(String name, String val) {
    setAttribute(name, val)
  }
}
