package rankspin.util;

import groovy.transform.CompileStatic;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.LocalDateTime;

// We don't use this currently.  Tried to use this so that entity Ids could be of type
// LocalDateTime, but there seems to be a bug in eclipse JPA such that this class is used
// with our custom SequenceGenerator.  However, the sequence generator must return a type
// that is the same as the id field of the entity, but then convertToEntityAttribute is
// Called on that converted type which causes a cast exception.

@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Timestamp> {
  @Override
  public Timestamp convertToDatabaseColumn(LocalDateTime ldt) {
    if (ldt == null) return null;
    Timestamp ts = Timestamp.valueOf(ldt);
    return ts;
  }

  @Override
  public LocalDateTime convertToEntityAttribute(Timestamp tstamp) {
    if (tstamp == null) return null;
    return tstamp.toLocalDateTime();
  }
}
