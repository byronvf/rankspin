package rankspin.util

import groovy.transform.CompileStatic

import java.text.SimpleDateFormat
import java.util.logging.LogRecord
import java.util.logging.SimpleFormatter

@CompileStatic
class MyLogFormatter extends SimpleFormatter {
//  private dformat = new SimpleDateFormat("yy-MM-dd HH:MM:ss.SSS")
  @Override
  String format(LogRecord rec) {
//    Date date = new Date(rec.millis)
//    String dstr = dformat.format(date)
//    String lname = rec.loggerName.substring(rec.loggerName.indexOf)

    // EclipseLink adds annoying file/connection info to the end of the logger name after
    // a colon, so we take it out here
    int idx = rec.loggerName.indexOf(':')
    if (idx == -1 )
      return "$rec.millis ${rec.level.name.substring(0, 4)} ${rec.loggerName} $rec.message \n"
    else
      return "$rec.millis ${rec.level.name.substring(0, 4)} ${rec.loggerName.substring(0, idx)} $rec.message \n"
  }
}

