package rankspin.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.OffsetDateTime;

// For some reason eclipse requires this to be a java file or it won't get
// loaded.

@Converter(autoApply = true)
public class OffsetDateTimeConverter implements AttributeConverter<OffsetDateTime, Timestamp> {
  @Override
  public Timestamp convertToDatabaseColumn(OffsetDateTime odt) {
    if (odt == null) return null;
    Timestamp ts = Timestamp.valueOf(odt.toLocalDateTime());
    return ts;
  }

  @Override
  public OffsetDateTime convertToEntityAttribute(Timestamp tstamp) {
    if (tstamp == null) return null;
    return OffsetDateTime.of(tstamp.toLocalDateTime(), OffsetDateTime.now().getOffset()) ;
  }
}

//@Converter(autoApply = true)
//public class OffsetDateTimeConverter implements
//    AttributeConverter<OffsetDateTime, String> {
//
//  /**
//   * @return a value as a String such as 2014-12-03T10:15:30+01:00
//   * @see OffsetDateTim∫#toString()
//   */
//  @Override
//  public String convertToDatabaseColumn(OffsetDateTime entityValue) {
//    return Objects.toString(entityValue, null);
//  }
//
//  @Override
//  public OffsetDateTime convertToEntityAttribute(String databaseValue) {
//    return OffsetDateTime.parse(databaseValue);
//  }
//}