package rankspin.entity

import javax.persistence.Entity
import java.sql.Timestamp

@Entity
class Game extends DbObj {
  @EntityId Timestamp id
  String name
}
