package rankspin.entity

import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import java.sql.Timestamp
import java.time.OffsetDateTime

@Entity
class Event extends DbObj {
  @EntityId Timestamp id
  int type = 1
  int numPlayers = 0
  OffsetDateTime dtime = OffsetDateTime.now()
  int winScore = 0
  int LooseScore = 0
  @OneToMany List<PlayerEvent> playerEvents
  @ManyToOne Game game
}
