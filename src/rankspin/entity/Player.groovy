package rankspin.entity

import jskills.IPlayer

import javax.persistence.Entity
import java.sql.Timestamp

@Entity
class Player extends DbObj implements IPlayer {
  @EntityId Timestamp id
  String email = "foo@email.com"
  String firstname
  String lastname
  enum Gender {UNSET, Male, Female}
  Gender gender = Gender.UNSET
  String nickname
}
