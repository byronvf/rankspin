package rankspin.entity

import groovy.transform.AnnotationCollector
import groovy.transform.CompileStatic
import groovy.util.logging.Log
import org.eclipse.persistence.internal.databaseaccess.Accessor
import org.eclipse.persistence.internal.sessions.AbstractSession
import org.eclipse.persistence.sequencing.Sequence

import javax.persistence.GeneratedValue
import javax.persistence.Id
import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.ZoneId

@Id
//@GeneratedValue(strategy=GenerationType.IDENTITY)
@GeneratedValue(generator = "id_gen")
@AnnotationCollector
@interface EntityId {}

class DbObj {
}

@Log
@CompileStatic
class TimestampSequence extends Sequence {

  TimestampSequence(String name) {
    super(name)
    assert lastTimestamp.nano % 1000000 == 0, "Timestamp has higher resolution then milliseconds"
  }

  @Override
  boolean shouldUseTransaction() {
    return false
  }

  @Override
  void onConnect() {

  }

  @Override
  void onDisconnect() {

  }

  @Override
  boolean shouldAcquireValueAfterInsert() {
    return false
  }

  @Override
  boolean shouldUsePreallocation() {
    return false;
  }

  private final ZoneId ZoneGMT7 = ZoneId.of("GMT-7")
  private final int IDCNT_LOW_RANGE = 0
  private final int IDCNT_HI_RANGE = 999
  private int idCnt = IDCNT_LOW_RANGE;
  private LocalDateTime lastTimestamp = LocalDateTime.now(ZoneGMT7)
  private synchronized Timestamp getTimestampId() {
    def ts = LocalDateTime.now(ZoneGMT7)
    if (ts.isAfter(lastTimestamp)) {
      idCnt = IDCNT_LOW_RANGE
    }
    else if (ts.isBefore(lastTimestamp)) {
      log.warning("Time went backward ${lastTimestamp} -> ${ts}")
      ts = lastTimestamp
    }

    if (idCnt > IDCNT_HI_RANGE) {
      idCnt = IDCNT_LOW_RANGE
      ts.plusNanos(1000000) // Increment by 1 millisecond
      log.warning("Advancing timestamp to make room for ids")
    }

    lastTimestamp = ts;
    ts = ts.plusNanos(idCnt*1000) // Adjust microsecond units
    idCnt++
    return Timestamp.valueOf(ts)
  }

  @Override
  Object getGeneratedValue(Accessor accessor, AbstractSession abstractSession, String s) {
    return getTimestampId()
  }

  @Override
  Vector getGeneratedVector(Accessor accessor, AbstractSession abstractSession, String s, int i) {
    throw new RuntimeException("Should not get here, method shouldUsePreallocation should relturn false")
  }
}