package rankspin.entity

import groovy.transform.ToString

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne
import java.time.OffsetDateTime

@Entity @ToString
class PlayerEvent extends DbObj {
  enum Result {NOTSET, WIN, LOOSE}
  enum Ack {NOACK, ACK}
  @Id @ManyToOne Event event
  @Id @ManyToOne Player player
  Result result = Result.NOTSET
  Ack ack = Ack.NOACK
  OffsetDateTime ackTime
}
