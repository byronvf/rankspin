package rankspin

import groovy.util.logging.Log
import rankspin.minion.Minion
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.handler.AbstractHandler
import rankspin.util.MyServletRequest
import rankspin.util.ThrowablePrinter

import javax.jws.WebService
import javax.servlet.ServletConfig
import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.util.concurrent.ConcurrentHashMap

@Log
@WebServlet(name="dispatch", urlPatterns = "/do/*")
class Dispatch extends HttpServlet {

  static final BASE_PATH = "/"
  static final REQ_MINION_NAME = "req_minion_name"

  private minionMgr = new MinionManager();

  @Override
  void init(ServletConfig config) throws ServletException {
    log.info("Dispatch Servlet loaded")
    super.init(config)
  }

  @Override
  protected void service(HttpServletRequest req, HttpServletResponse res) {
    def myReq = new MyServletRequest(req)
    log.fine("Dispatch request for '${req.pathInfo}'")
    assert req.pathInfo.indexOf(BASE_PATH) == 0, "The prefix of target URI must match BASE_PATH, e.g. /foo -> /foo/bar"
    def minionName = req.pathInfo.substring(BASE_PATH.size())
    def minion = minionMgr.getMinion(minionName)
    if (!minion) {
      myReq[REQ_MINION_NAME] = minionName
      minion = minionMgr.getMinion("Default")
      assert minion != null, "The default rankspin minion did not load"
    }
    try {
      minion.go(myReq, res)
    } catch (Exception e) {
      log.severe((new ThrowablePrinter(e)).toString())
      throw e
    }
  }

  void handle(String target, Request base, HttpServletRequest req, HttpServletResponse res) {
    def myReq = new MyServletRequest(req)
    assert target.indexOf(BASE_PATH) == 0, "The prefix of target URI must match BASE_PATH, e.g. /foo -> /foo/bar"
    def minionName = target.substring(BASE_PATH.size())
    def minion = minionMgr.getMinion(minionName)
    if (!minion) {
      myReq[REQ_MINION_NAME] = minionName
      log.severe("Minion '${minionName} not found, using 'Default'")
      minion = minionMgr.getMinion("Default")
      assert minion != null, "The default rankspin minion did not load"
    }
    try {
      minion.go(myReq, res)
    } catch (Exception e) {
      log.severe((new ThrowablePrinter(e)).toString())
      throw e
    }
    base.setHandled(true)
  }
}

@Log
class MinionManager {

  final minionPackage = 'rankspin.minion.'
  private minions = new ConcurrentHashMap<String, Minion>()
  def getMinion(String name) {
    assert name && name.size() > 1, "Minion name must nut be null, and longer then one character"
    def minion = minions[name]
    if ( ! minion) {
      def classname = minionPackage + name.substring(0,1).capitalize() + name.substring(1)
      try {
        minion = (Minion)Class.forName(classname).newInstance()
        minions[name] = minion
        log.info "Added Minion '$classname' with name '$name'"
      } catch (ClassNotFoundException e) {
        log.severe "class '$classname' with name '$name' not found when looking for a rankspin.minion"
      } catch (ClassCastException e) {
        log.severe "Found class '$classname' for name '$name' but it's not a Minion"
      }
    }
    return minion
  }

}