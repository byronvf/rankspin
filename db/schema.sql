--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: event; Type: TABLE; Schema: public; Owner: byron; Tablespace: 
--

CREATE TABLE event (
    type smallint NOT NULL,
    num_players smallint NOT NULL,
    dtime timestamp with time zone NOT NULL,
    win_score integer,
    loose_score integer,
    id timestamp without time zone NOT NULL,
    game_id timestamp without time zone NOT NULL
);


ALTER TABLE public.event OWNER TO byron;

--
-- Name: game; Type: TABLE; Schema: public; Owner: byron; Tablespace: 
--

CREATE TABLE game (
    name text NOT NULL,
    id timestamp without time zone NOT NULL
);


ALTER TABLE public.game OWNER TO byron;

--
-- Name: player; Type: TABLE; Schema: public; Owner: byron; Tablespace: 
--

CREATE TABLE player (
    email text,
    gender smallint DEFAULT 1 NOT NULL,
    nickname text,
    id timestamp without time zone NOT NULL,
    firstname text,
    lastname text
);


ALTER TABLE public.player OWNER TO byron;

--
-- Name: player_event; Type: TABLE; Schema: public; Owner: byron; Tablespace: 
--

CREATE TABLE player_event (
    result smallint NOT NULL,
    ack smallint DEFAULT 0 NOT NULL,
    ack_time timestamp with time zone,
    event_id timestamp without time zone NOT NULL,
    player_id timestamp without time zone NOT NULL
);


ALTER TABLE public.player_event OWNER TO byron;

--
-- Name: event_pkey; Type: CONSTRAINT; Schema: public; Owner: byron; Tablespace: 
--

ALTER TABLE ONLY event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);


--
-- Name: game_pkey; Type: CONSTRAINT; Schema: public; Owner: byron; Tablespace: 
--

ALTER TABLE ONLY game
    ADD CONSTRAINT game_pkey PRIMARY KEY (id);


--
-- Name: player_event_pkey; Type: CONSTRAINT; Schema: public; Owner: byron; Tablespace: 
--

ALTER TABLE ONLY player_event
    ADD CONSTRAINT player_event_pkey PRIMARY KEY (event_id, player_id);


--
-- Name: player_pkey; Type: CONSTRAINT; Schema: public; Owner: byron; Tablespace: 
--

ALTER TABLE ONLY player
    ADD CONSTRAINT player_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: byron
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM byron;
GRANT ALL ON SCHEMA public TO byron;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

